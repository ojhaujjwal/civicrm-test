<?php

class CRM_ContactPCP_BAO_ContactPCP
{
    public static function count($contactId) {
        if (!$contactId) {
            return 0;
        }

        return CRM_Core_DAO::singleValueQuery("
            SELECT COUNT(*) FROM civicrm_pcp where contact_id = '$contactId'
        ");
    }
}
