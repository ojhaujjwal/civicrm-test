<?php

class CRM_ContactPCP_Page_ContactPCP extends CRM_Core_Page
{
    function run() {
        $contactId = CRM_Utils_Request::retrieve( 'cid', 'Positive', $this, true );
        $this->assign( 'contactId', $contactId );
        // check logged in url permission
        require_once 'CRM/Contact/Page/View.php';
        CRM_Contact_Page_View::checkUserPermission( $this, $contactId );

        $status = CRM_PCP_BAO_PCP::buildOptions('status_id', 'create');
        $pages = array();
        $pcpSummary = array();

        // get all contribution pages
        $query = "SELECT id, title, start_date, end_date FROM civicrm_contribution_page WHERE (1)";
        $cpages = CRM_Core_DAO::executeQuery($query);
        while ($cpages->fetch()) {
            $pages['contribute'][$cpages->id]['id'] = $cpages->id;
            $pages['contribute'][$cpages->id]['title'] = $cpages->title;
            $pages['contribute'][$cpages->id]['start_date'] = $cpages->start_date;
            $pages['contribute'][$cpages->id]['end_date'] = $cpages->end_date;
        }

        $query = "SELECT id, title, start_date, end_date, registration_start_date, registration_end_date
                  FROM civicrm_event
                  WHERE is_template IS NULL OR is_template != 1";
        $epages = CRM_Core_DAO::executeQuery($query);
        while ($epages->fetch()) {
            $pages['event'][$epages->id]['id'] = $epages->id;
            $pages['event'][$epages->id]['title'] = $epages->title;
            $pages['event'][$epages->id]['start_date'] = $epages->registration_start_date;
            $pages['event'][$epages->id]['end_date'] = $epages->registration_end_date;
        }

        $query = "
            SELECT cp.id, cp.status_id, cp.title, cp.is_active, cp.page_type, cp.page_id, cp.goal_amount, cp.currency,
              SUM(cc.total_amount) as total_amount,
              COUNT(cc.id) as total_contributions
            FROM civicrm_pcp cp
            LEFT JOIN civicrm_contribution_soft cs ON ( cp.id = cs.pcp_id )
            LEFT JOIN civicrm_contribution cc ON ( cs.contribution_id = cc.id AND cc.contribution_status_id =1 AND cc.is_test = 0)
            WHERE cp.contact_id = '%1'
            GROUP by cp.id
            ORDER BY cp.status_id";

        $pcp = CRM_Core_DAO::executeQuery($query, array(1 => array($contactId, 'Integer')));

        while ($pcp->fetch()) {
            $page_type = $pcp->page_type;
            $page_id = (int) $pcp->page_id;
            if ($pages[$page_type][$page_id]['title'] == '' || $pages[$page_type][$page_id]['title'] == NULL) {
                $title = '(no title found for ' . $page_type . ' id ' . $page_id . ')';
            }
            else {
                $title = $pages[$page_type][$page_id]['title'];
            }

            if ($pcp->page_type == 'contribute') {
                $pageUrl = CRM_Utils_System::url('civicrm/' . $page_type . '/transact', 'reset=1&id=' . $pcp->page_id);
            }
            else {
                $pageUrl = CRM_Utils_System::url('civicrm/' . $page_type . '/register', 'reset=1&id=' . $pcp->page_id);
            }

            $pcpSummary[$pcp->id] = array(
                'id' => $pcp->id,
                'status_id' => $status[$pcp->status_id],
                'page_id' => $page_id,
                'page_title' => $title,
                'page_url' => $pageUrl,
                'page_type' => $page_type,
                'title' => $pcp->title,
                'total_amount' => $pcp->total_amount,
                'goal_amount' => $pcp->goal_amount,
                'currency' => $pcp->currency,
                'total_contributions' => $pcp->total_contributions
            );
        }

        $this->assign('rows', $pcpSummary);

        parent::run();
    }
}
