{if $rows}
  <div id="ltype">
    {strip}
      <table id="options" class="display">
        <thead>
        <tr>
          <th>{ts}Page Title{/ts}</th>
          <th>{ts}Contribution Page / Event{/ts}</th>
          <th>{ts}Goal Amount{/ts}</th>
          <th>{ts}Total Amount{/ts}</th>
          <th>{ts}Total Contributions{/ts}</th>
          <th>{ts}Status{/ts}</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$rows item=row}
          <tr id="row_{$row.id}">
            <td><a href="{crmURL p='civicrm/pcp/info' q="reset=1&id=`$row.id`" fe='true'}" title="{ts}View Personal Campaign Page{/ts}" target="_blank">{$row.title}</a></td>
            <td><a href="{$row.page_url}" title="{ts}View page{/ts}" target="_blank">{$row.page_title}</td>
            <td>{$row.goal_amount|crmMoney}</td>
            <td>{$row.total_amount|crmMoney}</td>
            <td>{$row.total_contributions}</td>
            <td>{$row.status_id}</td>
            <td>
              <span>
                <a class="action-item crm-hover-button" href="{crmURL p='civicrm/pcp/info' q="action=update&reset=1&id=`$row.id`" fe='true'}" title="{ts}Edit Personal Campaign Page{/ts}" target="_blank">{ts}Edit{/ts}</a>
              </span>
            </td>
          </tr>
        {/foreach}
        </tbody>
      </table>
    {/strip}
  </div>
{else}
  <div class="messages status no-popup">
    <div class="icon inform-icon"></div>
    {ts}There are no Personal Campaign Pages created by the contact.{/ts}
  </div>
{/if}
